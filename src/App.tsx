import Notify from "./components/Notify";
import Content from "./components/Content";

function App() {
  return (
    <>
      <Notify />
      <Content />
    </>
  );
}

export default App;
