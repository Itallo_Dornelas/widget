import { Flex, Image, Box, Text, Button } from "@chakra-ui/react";
import AvatarRed from "../assets/images/avatar--red.svg";
import AvatarOrange from "../assets/images/avatar--orange.svg";
const CardNotify = () => {
  return (
    <>
      <Flex
        p={4}
        bg="gray1.400"
        mt="4"
        alignItems="flex-start"
        borderRadius="12"
        _hover={{
          bg: "#252525",
        }}
      >
        <Image src={AvatarRed} />
        <Box>
          <Text
            w="80%"
            fontSize="0.85rem"
            lineHeight=" 1.25rem"
            fontWeight="600"
            color="textGray.600"
            pb={2}
            ml={5}
          >
            Seu <b>vocabulário personalizado</b> tem uma nova versão disponível!
          </Text>
          <Text
            w="100%"
            fontSize="0.7rem"
            lineHeight=" 1.25rem"
            fontWeight="500"
            color="textGray.600"
            pb={4}
            ml={5}
          >
            11 minutos atrás
          </Text>
          <Text
            w="85%"
            fontSize="0.85rem"
            lineHeight=" 1.25rem"
            fontWeight="500"
            color="textGray.600"
            pb={8}
            ml={5}
          >
            A nova versão foi gerada a partir de <b> 120h</b> de gravações da
            sua voz e garante uma maior precisão na transcrição.
          </Text>
          <Button
            w="90%"
            bg="red.600"
            _hover={{
              bg: "redLight.500",
            }}
            ml={5}
          >
            <Text>Utilizar a nova versão</Text>
          </Button>
        </Box>
      </Flex>
      <Flex
        p={4}
        bg="gray1.400"
        mt="4"
        alignItems="flex-start"
        borderRadius="12"
        _hover={{
          bg: "#252525",
        }}
      >
        <Image src={AvatarOrange} />
        <Box>
          <Text
            w="80%"
            fontSize="0.85rem"
            lineHeight=" 1.25rem"
            fontWeight="600"
            color="textGray.600"
            pb={2}
            ml={5}
          >
            Suas últimas gravações estão com <b>bastante ruído</b> no microfone!
          </Text>
          <Text
            w="100%"
            fontSize="0.7rem"
            lineHeight=" 1.25rem"
            fontWeight="500"
            color="textGray.600"
            pb={4}
            ml={5}
          >
            23 minutos atrás
          </Text>
          <Text
            w="85%"
            fontSize="0.85rem"
            lineHeight=" 1.25rem"
            fontWeight="500"
            color="textGray.600"
            ml={5}
          >
            O ruído <b>atrapalha</b> a precisão da transcrição, podendo causar
            erros no seu laudo. Tente ir para um lugar mais silencioso.
          </Text>
        </Box>
      </Flex>
    </>
  );
};
export default CardNotify;
