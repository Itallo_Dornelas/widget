import { Container } from "@chakra-ui/react";
import TemplateRecord from "../template/TemplateRecord";
import Icons from "./Icons";
import Record from "./Record";

const Content = () => {
  return (
    <TemplateRecord>
      <Container>
        <Record />
        <Icons />
      </Container>
    </TemplateRecord>
  );
};

export default Content;
