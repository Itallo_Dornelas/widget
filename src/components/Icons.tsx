import { Button, Flex, Image, Text } from "@chakra-ui/react";
import Mic from "../assets/images/microphone.svg";
import Conf from "../assets/images/settings.svg";
import Voc from "../assets/images/vocabulary.svg";
import Activy from "../assets/images/activity.svg";
import Notify from "../assets/images/notification.svg";
import { useShow } from "../provider/show/show";
const Icons = () => {
  const { show, handleClose, handleShow } = useShow();
  return (
    <Flex mt={4} justifyContent="space-around" padding="0px">
      <Button
        display="flex"
        flexDirection="column"
        bg="gray2.500"
        size="sm"
        p={4}
        mb={4}
        h="60px"
        _hover={{
          bg: "gray1.400",
          borderRadius: "5",
        }}
        type="button"
      >
        <Image src={Mic} />
        <Text
          color="textGray.600"
          fontSize="0.8125rem"
          lineHeight="1.25rem"
          fontWeight="500"
        >
          Microfone
        </Text>
      </Button>
      <Button
        display="flex"
        flexDirection="column"
        bg="gray2.500"
        size="sm"
        p={4}
        mb={4}
        h="60px"
        _hover={{
          bg: "gray1.400",
          borderRadius: "5",
        }}
        type="button"
      >
        <Image src={Conf} />
        <Text
          color="textGray.600"
          fontSize="0.8125rem"
          lineHeight="1.25rem"
          fontWeight="500"
        >
          Configurações
        </Text>
      </Button>
      <Button
        display="flex"
        flexDirection="column"
        bg="gray2.500"
        size="sm"
        p={4}
        mb={4}
        h="60px"
        _hover={{
          bg: "gray1.400",
          borderRadius: "5",
        }}
        type="button"
      >
        <Image src={Activy} />
        <Text
          color="textGray.600"
          fontSize="0.8125rem"
          lineHeight="1.25rem"
          fontWeight="500"
        >
          Status
        </Text>
      </Button>
      <Button
        display="flex"
        flexDirection="column"
        bg="gray2.500"
        size="sm"
        p={4}
        mb={4}
        h="60px"
        _hover={{
          bg: "gray1.400",
          borderRadius: "5",
        }}
        type="button"
      >
        <Image src={Voc} />
        <Text
          color="textGray.600"
          fontSize="0.8125rem"
          lineHeight="1.25rem"
          fontWeight="500"
        >
          Vocabulário
        </Text>
      </Button>
      <Button
        display="flex"
        flexDirection="column"
        bg="gray2.500"
        size="sm"
        p={4}
        mb={4}
        h="60px"
        onClick={show ? handleClose : handleShow}
        _hover={{
          bg: "gray1.400",
          borderRadius: "5",
        }}
        type="button"
      >
        <Image src={Notify} />
        <Text
          color="textGray.600"
          fontSize="0.8125rem"
          lineHeight="1.25rem"
          fontWeight="500"
        >
          Notificações
        </Text>
      </Button>
    </Flex>
  );
};
export default Icons;
