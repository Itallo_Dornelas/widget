import { Container, Text, Button, Flex, Box } from "@chakra-ui/react";
import { useShow } from "../provider/show/show";
import TemplateNotify from "../template/TemplateNotify";
import CardNotify from "./CardNotify";

const Notify = () => {
  const { showNotify, setShowNotify } = useShow();
  return (
    <TemplateNotify>
      <Container padding="0 0 25px 0">
        <Flex justifyContent="space-between">
          <Text fontSize="1rem" fontWeight="600" lineHeight="1.25rem">
            Notificações
          </Text>
          <Button
            bg="gray2.500"
            size="xs"
            onClick={() => setShowNotify(true)}
            _hover={{
              bg: "gray1.400",
              borderRadius: "5",
            }}
          >
            <Text
              color="textGray.600"
              fontSize="0.8125rem"
              lineHeight="1.25rem"
              fontWeight="500"
            >
              Marcar todas como lidas
            </Text>
          </Button>
        </Flex>
        {!showNotify ? (
          <CardNotify />
        ) : (
          <Flex
            p={4}
            bg="gray1.400"
            mt="8"
            alignItems="flex-start"
            borderRadius="12"
            _hover={{
              bg: "#252525",
            }}
          >
            <Box>
              <Text
                fontSize="0.85rem"
                lineHeight=" 1.25rem"
                fontWeight="600"
                textAlign="center"
                color="textGray.600"
                w="100%"
              >
                Você não possui mais nenhuma notificação
              </Text>
            </Box>
          </Flex>
        )}
      </Container>
    </TemplateNotify>
  );
};

export default Notify;
