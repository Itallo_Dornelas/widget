import { ReactMic } from "react-mic";
import { useState } from "react";
import { Button, Flex, Image } from "@chakra-ui/react";
import Mic from "../assets/images/microphone--large.svg";
import MicAtiv from "../assets/images/activity.svg";
const Record = () => {
  const [record, setRecord] = useState(false);
  const startRecording = () => {
    setRecord(true);
  };

  const stopRecording = () => {
    setRecord(false);
  };

  return (
    <Flex alignItems="center" justifyContent="space-between">
      <ReactMic
        record={record}
        pause={record}
        visualSetting="frequencyBars"
        backgroundColor="#3A3A3A"
        mimeType="audio/webm"
        noiseSuppression={true}
      />

      <Button
        width="30px"
        bg="red.600"
        _hover={{
          bg: "redLight.500",
        }}
        onClick={!record ? startRecording : stopRecording}
        type="button"
      >
        <Image maxWidth="none" src={!record ? Mic : MicAtiv} />
      </Button>
    </Flex>
  );
};
export default Record;
