import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { extendTheme } from "@chakra-ui/react";
import { ChakraProvider } from "@chakra-ui/react";
import { Theme } from "./theme/default";
import Provider from "../src/provider/";
const theme = extendTheme(Theme);
ReactDOM.render(
  <React.StrictMode>
    <Provider>
      <ChakraProvider theme={theme}>
        <App />
      </ChakraProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
