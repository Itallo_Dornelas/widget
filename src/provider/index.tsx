import { ReactNode } from "react";
import { ShowProvider } from "./show/show";

interface ProvidersProps {
  children: ReactNode;
}
const Providers = ({ children }: ProvidersProps) => {
  return <ShowProvider>{children}</ShowProvider>;
};

export default Providers;
