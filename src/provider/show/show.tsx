import {
  createContext,
  ReactNode,
  useContext,
  useState,
  SetStateAction,
  Dispatch,
} from "react";

interface ShowProviderData {
  show: boolean;
  setShow: Dispatch<SetStateAction<boolean>>;
  setShowNotify: Dispatch<SetStateAction<boolean>>;
  showNotify: boolean;
  handleClose: () => void;
  handleShow: () => void;
}

interface ShowProviderProps {
  children: ReactNode;
}

export const ShowContext = createContext<ShowProviderData>(
  {} as ShowProviderData
);

export const ShowProvider = ({ children }: ShowProviderProps) => {
  const [show, setShow] = useState(false);
  const [showNotify, setShowNotify] = useState(false);
  const handleClose = () => {
    setShow(false);
  };
  const handleShow = () => {
    setShow(true);
  };

  return (
    <ShowContext.Provider
      value={{
        show,
        setShow,
        handleClose,
        handleShow,
        showNotify,
        setShowNotify,
      }}
    >
      {children}
    </ShowContext.Provider>
  );
};

export const useShow = () => useContext(ShowContext);
