import { Button, Container, Collapse, Flex, Image } from "@chakra-ui/react";
import Notify from "../assets/images/notification.svg";
import Close from "../assets/images/close.svg";
import { useShow } from "../provider/show/show";
const TemplateNotify: React.FC = ({ children }) => {
  const { show, handleClose, handleShow } = useShow();

  return (
    <>
      <Container maxW="md" mt="4" borderRadius="12px" pt={5} bg="gray2.500">
        <Flex
          justifyContent="flex-end"
          w="96%"
          alignItems="center"
          centerContentdirection="column"
        >
          <Button
            mb="4"
            bg="gray2.500"
            size="xs"
            onClick={show ? handleClose : handleShow}
            _hover={{
              bg: "gray1.400",
              borderRadius: "5",
            }}
          >
            <Image src={show ? Close : Notify} alt="Close" />
          </Button>
        </Flex>
        <Collapse animateOpacity in={show}>
          {show && children}
        </Collapse>
      </Container>
    </>
  );
};

export default TemplateNotify;
