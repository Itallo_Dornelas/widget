import { Container, Flex } from "@chakra-ui/react";

const TemplateRecord: React.FC = ({ children }) => {
  return (
    <>
      <Container maxW="md" mt="5" borderRadius="12px" pt={5} bg="gray2.500">
        <Flex
          justifyContent="flex-end"
          w="96%"
          alignItems="center"
          centerContentdirection="column"
        />
        {children}
      </Container>
    </>
  );
};

export default TemplateRecord;
