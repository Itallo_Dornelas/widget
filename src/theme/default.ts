import { Theme as ChakraTheme, theme } from "@chakra-ui/react";

export const Theme = {
  ...theme,
  styles: {
    global: {
      body: {
        margin: 0,
        padding: 0,
        boxSizing: "border-box",
        outline: 0,
        fontFamily: "Roboto",
        bg: "#131313",
        color: "#fff",
      },
      b: {
        color: "textWhite.600",
        fontWeigth: "500",
      },
      canvas: {
        width: "80%",
        borderRadius: "12",
      },
    },
  },
  config: {
    initialColorMode: "dark",
    useSystemColorMode: false,
  },

  colors: {
    ...theme.colors,
    gray0: {
      ...theme.colors.gray,
      300: "#0E0E0E",
    },
    gray1: {
      ...theme.colors.gray,
      400: "#212121",
    },

    gray2: {
      ...theme.colors.gray,
      500: "#2B2B2C",
    },

    gray3: {
      ...theme.colors.gray,
      600: "#3A3A3A",
    },

    red: {
      ...theme.colors.red,
      600: "#FF4757",
    },

    redLight: {
      ...theme.colors.red,
      500: "#FFA3AF",
    },

    textWhite: {
      ...theme.colors.gray,
      600: "#FFFFFF",
    },

    textGray: {
      ...theme.colors.gray,
      600: "#A0A0A0",
    },
  },
} as ChakraTheme;
